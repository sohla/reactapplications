import React, {Component} from 'react';
import PropTypes from 'prop-types';


export class Addtodo extends Component {

  state = {
    title: ''
  }

  onSubmit = (e) => {
    e.preventDefault();
    this.props.Addtodo(this.state.title);
    this.setState({title: ""});
  }

  onChange = (e) => this.setState({title: e.target.value});


  render() {
    return (
      <form onSubmit={this.onSubmit} style ={{display: 'flex'}}>
        <input
          type="text"
          name="title"
          placeholder="add to do..."
          style={{flex:'10'}}
          value={this.state.title}
          onChange={this.onChange}
        />
        <input
          type="submit"
          value="Submit"
          className="btn"
          style={{flex: '1'}}/>
      </form>
    )
  }


}


export default Addtodo
