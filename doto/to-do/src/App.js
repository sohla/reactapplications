import React, {Component} from 'react';
import Todos from './components/Todos';
import Addtodo from './components/Addtodo';
import Header from './components/layout/Header';
import uuid from 'uuid';

import './App.css';

class App extends Component {
  state = {
    todos: [
        {
            id: uuid.v4(),
            title : 'Take ou the trash',
            completed: false

        },
        {
            id: uuid.v4(),
            title : 'Dinner',
            completed: false

        },
        {
            id: uuid.v4(),
            title : 'Meeting',
            completed: false

        }

    ]
  }

  markComplete = (id) => {
    this.setState({
      todos: this.state.todos.map(todo => {
        if (todo.id === id) {
          todo.completed = !todo.completed;
        }
        return todo;
      })
    });
  };

  delTodo = (id) => {
    this.setState({todos: [...this.state.todos.filter(todo=>todo.id !== id)]});
  }

  addTodo = (title) => {
    const newTodo = {
      id: uuid.v4(),
      title : title,
      commeted: false
    }
    this.setState({todos: [...this.state.todos, newTodo]})
  }

  render() {
    return (
      <div className="App">
      <div className="container">
          <h1>app</h1>
          <Addtodo Addtodo={this.addTodo}/>
          <Todos todos={this.state.todos} markComplete={this.markComplete} delTodo={this.delTodo}/>
      </div>
      </div>
    );
  }
}
export default App;
