import React, {Component} from 'react';

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      data: 'sohla'
    }
    this.handleEvent = this.handleEvent.bind(this);
  }
    handleEvent = () => {
      console.log(this.props);
    }
    render(){
      return (

        <div className="App">
          <input type="text" value={this.state.data}/>
            <button onClick={this.handleEvent}>Click</button>
        </div>

      );
    }
}

export default App;
