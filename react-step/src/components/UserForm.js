import React, { Component } from 'react'
import FormUserDetails from './FormUserDetails'
import FormPersonDetails from './FormPersonDetails'
import Confirm from './Confirm'
import Sucess from './Success.js'


export class Class extends Component {

  state = {
    step: 1,
    firstName: '',
    lastName: '',
    email: '',

    bio: '',
    city: ''
  };

  nextStep = () => {
    const { step } = this.state;
    this.setState({
      step: step + 1
    });
  };


  prevStep = () => {
    const {step} = this.step;
    this.setState({
      step: step - 1
    });
  };

  handleChange = input => e => {
    this.setState({[input]: e.target.value});
  };

  render() {
    const {step} = this.state;
    const {firstName, lastName, email, bio, city} = this.state;
    const values = {firstName, lastName, email, bio, city};
    switch (step) {
      case 1:
        return(
          <FormUserDetails
            nextStep={this.nextStep}
            handleChange={this.handleChange}
            values={values}
          />
        )
      case 2:
        return(
         <FormPersonDetails
           nextStep={this.nextStep}
           prevStep={this.backStep}
           handleChange={this.handleChange}
           values={values}
         />

        )
      case 3:
        return(
          <Confirm
            nextStep={this.nextStep}
            prevStep={this.backStep}
            values={values}
          />
        )
      case 4:
        return(
          <Sucess/>
        )


    }
  }
}

export default Class
