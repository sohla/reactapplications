import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';



export class FormPersonDetails extends Component {

  continue = e => {
    e.preventDefault();
    this.props.nextStep();
  }

  back = e => {
    e.preventDefault();
    this.props.prevStep();
  }

  render() {
    const {values, handleChange} = this.props;
    return (
      <MuiThemeProvider>
        <React.Fragment>
          <AppBar title="enter person details"/>
          <TextField
            hint="bio"
            floatingLabelText='bio'
            onChange={handleChange('bio')}
            defaultValue={values.bio}
          />
          <br/>
          <TextField
            hint="city"
            floatingLabelText='city'
            onChange={handleChange('city')}
            defaultValue={values.city}
          />
          <br/>
          <RaisedButton
            label="continue"
            primary={true}
            style={styles.button}
            onClick={this.continue}
          />
          <br/>
          <RaisedButton
            label="Back"
            primary={true}
            style={styles.button}
            onClick={this.back}
          />

        </React.Fragment>
      </MuiThemeProvider>
    )
  }
}

const styles = {
  button: {
    margin:15
  }
}

export default FormPersonDetails
