import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import {List, ListItem} from 'material-ui/List';
import RaisedButton from 'material-ui/RaisedButton';



export class Confirm extends Component {

  continue = e => {
    e.preventDefault();
    this.props.nextStep();
  }

  back = e => {
    e.preventDefault();
    this.props.prevStep();
  }

  render() {
    const {values: {firstName, lastName, email, bio, city}} = this.props;
    return (
      <MuiThemeProvider>
        <React.Fragment>
          <AppBar title="Confirm user data"/>
          <List>
            <ListItem
              primaryText ="first nam"
              secondaryText={firstName}
            />
            <ListItem
              primaryText ="last nam"
              secondaryText={lastName}
            />
            <ListItem
              primaryText ="email"
              secondaryText={email}
            />
            <ListItem
              primaryText ="bio"
              secondaryText={bio}
            />
            <ListItem
              primaryText ="city"
              secondaryText={city}
            />
          </List>
          <br/>
          <RaisedButton
            label="confirm & continue"
            primary={true}
            style={styles.button}
            onClick={this.continue}
          />
          <RaisedButton
            label="Back"
            primary={true}
            style={styles.button}
            onClick={this.back}
          />

        </React.Fragment>
      </MuiThemeProvider>
    )
  }
}

const styles = {
  button: {
    margin:15
  }
}

export default Confirm
